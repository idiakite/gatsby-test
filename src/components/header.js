import { Link } from "gatsby";
import PropTypes from "prop-types";
import React from "react";

const Header = ({ siteTitle }) => (
  <header
    style={{
      background: `#FF5733`,
      marginBottom: `1.45rem`,
    }}
  >
    <div
      style={{
        margin: `0 auto`,
        maxWidth: 960,
        padding: `1.45rem 1.0875rem`,
      }}
    >
      <h1 style={{ margin: 0 }}>
        <Link
          to="/"
          style={{
            color: `white`,
            textDecoration: `none`,
          }}
        >
          {siteTitle}
        </Link>
      </h1>
      <h2
        style={{
          color: `white`,
          textDecoration: `none`,
        }}
      >
        Sous-titre
      </h2>

      <nav style={{}}>
        <ul
          style={{
            display: `flex`,
            justifyContent: `flex-end`,
            listStyle: `none`,
            color: `white`,
          }}
        >
          <li
            style={{
              marginTop: `-50px`,
              marginLeft: `170px`,
              marginRight: `20px`,
            }}
          >
            <Link
              to="/"
              style={{
                color: `white`,
                textDecoration: `none`,
              }}
            >
              Home
            </Link>
          </li>
          <li
            style={{
              marginRight: `20px`,
              marginTop: `-50px`,
            }}
          >
            <Link
              to="/blog"
              style={{
                color: `white`,
                textDecoration: `none`,
              }}
            >
              Blog
            </Link>
          </li>
          <li
            style={{
              marginRight: `20px`,
              marginTop: `-50px`,
            }}
          >
            <Link
              to="/portofolio"
              style={{
                color: `white`,
                textDecoration: `none`,
              }}
            >
              Portofolio
            </Link>
          </li>
          <li
            style={{
              marginRight: `20px`,
              marginTop: `-50px`,
            }}
          >
            <Link
              to="/about"
              style={{
                color: `white`,
                textDecoration: `none`,
              }}
            >
              À propos
            </Link>
          </li>
          <li
            style={{
              marginRight: `20px`,
              marginTop: `-50px`,
            }}
          >
            <Link
              to="/contact"
              style={{
                color: `white`,
                textDecoration: `none`,
              }}
            >
              Contact
            </Link>
          </li>
        </ul>
      </nav>
    </div>
  </header>
);

Header.propTypes = {
  siteTitle: PropTypes.string,
};

Header.defaultProps = {
  siteTitle: ``,
};

export default Header;
