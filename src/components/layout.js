import React from "react";
import PropTypes from "prop-types";
import { useStaticQuery, graphql } from "gatsby";
import { FaFacebook, FaTwitter, FaInstagram } from "react-icons/fa";

import Header from "./header";
import "./layout.css";

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `);

  return (
    <>
      <Header siteTitle={data.site.siteMetadata?.title || `Title`} />
      <div
        style={{
          margin: `0 auto`,
          maxWidth: 960,
          padding: `0 1.0875rem 1.45rem`,
        }}
      >
        <main>{children}</main>

        <footer
          style={{
            marginTop: `2rem`,
          }}
        >
          © {new Date().getFullYear()}, réalisé avec amour (et sans trucages).
          <div>
            <ul>
              <li>
                <a href="https://facebook.com/" target="_blank">
                  <FaFacebook />
                </a>
              </li>
              <li>
                <a href="https://twitter.com/" target="_blank">
                  <FaTwitter />
                </a>
              </li>
              <li>
                <a href="https://instagram.com/" target="_blank">
                  <FaInstagram />
                </a>
              </li>
            </ul>
          </div>
        </footer>
      </div>
    </>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
