import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Not found" />
    <h1>404: Page introuvable</h1>
    <p>Tu fais fausse route.. Retourne donc avec le groupe !</p>
  </Layout>
)

export default NotFoundPage
