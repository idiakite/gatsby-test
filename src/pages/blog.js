import React from "react";
import { graphql, Link } from "gatsby";
import Layout from "../components/layout";
import SEO from "../components/seo";

export default function Home({ data }) {
  return (
    <Layout>
      <SEO title="Blog" />
      <div>
        <h1>BLOG</h1>

        <p style={{ marginTop: `-25px`, marginBottom: `50px` }}>
          Nombre total d'articles: {data.allMarkdownRemark.totalCount}
        </p>

        {data.allMarkdownRemark.edges.map(({ node }) => (
          <div key={node.id}>
            <Link
              to={node.fields.slug}
              style={{ textDecoration: `none`, color: `#FF5733` }}
            >
              <h3>{node.frontmatter.title}</h3>
            </Link>
            <p style={{ marginTop: `-15px`, marginBottom: `50px` }}>
              {node.excerpt}
            </p>
          </div>
        ))}
      </div>
    </Layout>
  );
}

export const query = graphql`
  query {
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "YYYY")
          }
          fields {
            slug
          }
          excerpt
          excerpt
        }
      }
    }
  }
`;
