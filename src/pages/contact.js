import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"

const Contact = () => (
  <Layout>
    <SEO title="Contact" />
    <img src="https://images.unsplash.com/uploads/141103282695035fa1380/95cdfeef?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=2548&q=80" alt=""></img>
    <h1>CONTACT</h1>
    <p>Vous avez une question ?</p>
    <ul>
      <li>Téléphone: 06.49.67.40.85</li>
      <li>
        Adresse mail:{" "}
        <a href="mailto:idriss.diakite@ultro.fr">coucou@ultro.fr</a>
      </li>
      <li>
        <a href="https://facebook.com/coucou">facebook</a> x 
        <a href="https://twitter.com/coucou"> twitter</a> x 
        <a href="https://instagram.com/coucou"> instagram</a> x 
        <a href="https://linkdin.com/coucou"> linkdin</a>
      </li>
    </ul>
  </Layout>
)

export default Contact
