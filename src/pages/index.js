import React from "react"
import { Link } from "gatsby"
import styles from "./index-module.css"
import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <div class="container">
      <img
        className="imgHome"
        src="https://images.unsplash.com/photo-1614285436350-45934638b0f3?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=3017&q=80" alt=""
      ></img>
      <div className="overlay">
        <h1 className="text">WELCOME</h1>
      </div>
    </div>

    <div></div>
    <Link to="/blog">Accéder au blog</Link> <br />
  </Layout>
)

export default IndexPage
