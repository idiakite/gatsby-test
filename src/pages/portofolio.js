import React from "react";
import { graphql } from "gatsby";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Img from "gatsby-image";

export default function Portofolio({ data }) {
  console.log(data.images.nodes[0]);
  return (
    <Layout>
      <SEO title="Portofolio" />
      <h1>PORTOFOLIO</h1>
      <div>
        {data.images.nodes.map((image) => (
          <Img
            style={{ margin: `10px` }}
            key={image.id}
            fixed={image.fixed}
            alt=""
          />
        ))}
      </div>
    </Layout>
  );
}

export const query = graphql`
  query {
    images: allImageSharp(
      filter: { id: { nin: "7e10f887-25e0-5590-810f-82713fb18688" } }
    ) {
      totalCount
      nodes {
        id
        fixed(width: 420, height: 350) {
          ...GatsbyImageSharpFixed
          src
        }
      }
    }
  }
`;
